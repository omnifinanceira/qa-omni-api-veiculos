require_relative '../pages/request.rb'
require_relative '../pages/util.rb'
require 'httparty'
require 'cpf_cnpj'
require 'faker'
require "json-schema"
require 'jsonpath'
require 'pry'
require 'yaml'

AMBIENTE = ENV['AMBIENTE']
CONFIG = YAML.load_file(File.dirname(__FILE__) + "/ambientes/#{AMBIENTE}.yml")

util = Util.new