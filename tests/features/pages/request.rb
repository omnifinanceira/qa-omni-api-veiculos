require_relative '../pages/util.rb'

class Request < Util

    def initialize
        @base_url = CONFIG['url_geral']
    end

    def exec_post_aut(body)
        url_full = "https://dev-omnifacil2.omni.com.br/desenv/pck_login.prc_valida_usuario"

        response = HTTParty.post(url_full,
        :body => body,
        :headers => {'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/x-www-form-urlencoded' })

        $cookie = response.headers['set-cookie']

        unless response['Erros'].nil?
            gravar_request_response(url_full,body,response)
        end

        return response
    end

   # {"login":"331LUANA", "token":"228551063"}
    def exec_post_aut_ws(body)
        
        url_full = "https://api.dev-omnicfi.us-east-1.omniaws.io/login-omnifacil/api/auth/user"
        
        response = HTTParty.post(url_full,
        :body => body.to_json,
        :headers => {'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8' })
       
        $cookie = response.headers['set-cookie']

        unless response['Erros'].nil?
            gravar_request_response(url_full,body,response)
        end

        return response
    end
  
    def exec_post(path,body,autenticacao)
        url_full = "#{@base_url}#{path}"

        response = HTTParty.post(url_full,
        :body => body.to_json,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            # 'omni-autenticacao' => {"login": "#{autenticacao['nome']}", "token": "#{autenticacao['ide']}", "agente": "#{autenticacao['emp']}"}.to_json })
            'omni-autenticacao' => {"login": "#{autenticacao['usuarioTO']['login']}", "token": "#{autenticacao['usuarioTO']['token']}", "agente": "#{autenticacao['usuarioTO']['agentes'][0]['id']}"}.to_json })

        unless response['Erros'].nil?
            gravar_request_response(url_full,body,response)
        end

      return response
    end

    def exec_get(path,autenticacao)
     
     # binding.pry
        url_full = "#{@base_url}#{path}"
         
        response = HTTParty.get(url_full,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            'omni-autenticacao' => {"login": "#{autenticacao['usuarioTO']['login']}", "token": "#{autenticacao['usuarioTO']['token']}", "agente": "#{autenticacao['usuarioTO']['agentes'][0]['id']}"}.to_json })
      return response
    end 
end