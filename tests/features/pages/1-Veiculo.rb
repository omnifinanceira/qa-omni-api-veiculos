class Veiculo < Request

    def lista_combustivel(autenticacao)

        combustivel = exec_get("/api/vehicles/fuel-types",autenticacao)

        return combustivel
    end

    def tipos_veiculo(autenticacao)

        tipos_veiculo = exec_get("/api/vehicles/types",autenticacao)

        return tipos_veiculo
    end
end



