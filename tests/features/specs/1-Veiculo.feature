#language:pt
@1 @veiculo
Funcionalidade: Veiculo

Contexto:
    Dado eu autentico


#/api/vehicles/fuel-types 
Cenario: Combustivel 
E faco a requisicao para tipos de combustivel
Entao valido o retorno tipos de combustivel

#/api/vehicles/types
Cenario: Tipo veiculo 
E faco a requisicao para tipos de veiculo
Entao valido o retorno tipos de veiculo
