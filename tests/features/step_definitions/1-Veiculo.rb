Quando('faco a requisicao para tipos de combustivel') do
    @veiculo = Veiculo.new
    @response_veiculo = @veiculo.lista_combustivel($autenticacao)
end

Entao('valido o retorno tipos de combustivel') do
     expect(@response_veiculo.code).to eq(200) 
end

Quando('faco a requisicao para tipos de veiculo') do
    @veiculo = Veiculo.new
    @response_veiculo = @veiculo.tipos_veiculo($autenticacao)
end

Entao('valido o retorno tipos de veiculo') do
     expect(@response_veiculo.code).to eq(200) 
end